#
# Copyright (C) 2023 The Android Open Source Project
# Copyright (C) 2023 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/omni_tb8185p3_64.mk

COMMON_LUNCH_CHOICES := \
    omni_tb8185p3_64-user \
    omni_tb8185p3_64-userdebug \
    omni_tb8185p3_64-eng
