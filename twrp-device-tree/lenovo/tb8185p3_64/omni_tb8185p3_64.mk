#
# Copyright (C) 2023 The Android Open Source Project
# Copyright (C) 2023 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Omni stuff.
$(call inherit-product, vendor/omni/config/common.mk)

# Inherit from tb8185p3_64 device
$(call inherit-product, device/lenovo/tb8185p3_64/device.mk)

PRODUCT_DEVICE := tb8185p3_64
PRODUCT_NAME := omni_tb8185p3_64
PRODUCT_BRAND := Lenovo
PRODUCT_MODEL := Lenovo TB-J616F
PRODUCT_MANUFACTURER := lenovo

PRODUCT_GMS_CLIENTID_BASE := android-lenovo

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="full_tb8185p3_64-user 12 SP1A.210812.016 TB-J616F_S240155_230210_ROW release-keys"

BUILD_FINGERPRINT := Lenovo/TB-J616F/TB-J616F:12/SP1A.210812.016/TB-J616F_S240155_230210_ROW:user/release-keys
